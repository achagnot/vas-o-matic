# Vas-O-Matic

VasOMatic is a ImageJ macro dedicated to analyze vascular coverage by pericytes on large fields of view. ChunkAnalyzer is a R script for viewing and preprocessing the output tables of VasOMatic.

<b>OVERVIEW</b>

The VasOMatic macro was written using FIJI (FIJI is Just ImageJ, 10.1038/nmeth.2019). First part of the algorithm is an interactive loop where users are invited to segment the vascular component of the image, using median smoothing, thresholding, and particle analysis to remove background noise and close holes in the mask. The vascular mask quality is critical as it will be the basis of the next steps. A second interactive loop aloows the user for segmenting the paravascular (e.g. pericytes, astrocyte endfeet, etc…) part of the image.
The second part of the algorithm splits the vascular mask into chunks using ImageJ’s watershed function. The length, diameter, coverage and paravascular signal are recovered individually for each chunk, using skeleton measurement, distance mapping and masks overlap, respectively.
Results are finally displayed in three windows. The individual chunks data are shown in ImageJ’s results window, vasculature characteristics are depicted as a cumulative graph of length vs. diameter, and the global results from the ROI, namely FOV dimensions, total vasculature length, diameter and coverages are presented in ImageJ’s log window.


<b>HOW TO USE VAS-O-MATIC</b>

<b>Introduction:</b> The VasOMatic macro aims to collect coverage data of one channel overlapping the vascular structure (pericytes in the provided example) and plot them in relation to local vascular diameter. It can also be used to asses general vascular morphology such as diameter distribution and vascular density. The macro works on ImageJ 1.53, using Java 1.8.0_322. Please update ImageJ and/or Java if any dysfunctions are observed.

* <b>Step 1</b>: Load the VasOMatic.ijm into ImageJ with Plugin>>Macro>>Install…

* <b>Step 2</b>: Open your image in ImageJ.

<i>Note</i>: Whole section files (e.g. .OME.TIFF) may be very large and difficult to handle without causing ImageJ to crash. Alternate softwares, e.g. QuPath, allow large 2D image handling and incorporate ImageJ.

* <b>Step 3</b>: Select the image. If it is a subfield selected within FIJI’s field of view, remove all exterior background by doing “Image>>Clear outside” prior to run the macro.

* <b>Step 4</b>: Press F1 to run the macro. Alternatively, use “Plugins>>Macros>>Run…”

<i>Note</i>: To change the bound key, open VasOMatic.ijm in a text editor and in the first line of the code replace “f1” by the key of your choice.

* <b>Step 5</b>: In the “Channel allocation” window, allocate the channels accordingly. Projection type is relevant only if for a z-stack and will apply the corresponding transform before treating the data, as the macro works on 2D images.

* <b>Step 6</b>: In the “Mask adjustments” window, specify the filtering options for vascular segmentation. Values are in pixel, and you will be able to change them if you are not satisfied with the segmentation run.

* <b>Step 7</b>: Use the “Threshold” window to adjust for the initial segmentation of the vasculature. You can observe the changes in the “Vessel premask” window. Once you are satisfied, press OK in the “Action Required” window.

* <b>Step 8</b>: Check on the “Vessel mask overlap” window that the mask (red) includes most of the vessels while leaving  the background (gray). If the segmentation is satisfying, press “Yes” and proceed to Step 9. Otherwise, press “No” to go back to Step 6.

* <b>Step 9</b>: Similarly to Step 6, use the “Threshold” window to segment the paravascular signal (e.g. pericytes). Press “OK” when done.

* <b>Step 10</b>: Similarly to Step 8, check on the “Paravascular mask overlap” window the quality of the segmentation. Pressing “No” will go back to Step 9, while pressing “Yes” allows the macro to proceed.

<i>Note</i>: The macro may need to run for some time (a few mins for large images) after this step. Wait until the results window appear.

* <b>Step 11</b>: Collect the data from the results windows and paste them in another file (eg Excel) for further analyses. General results are displayed in the “VAS-O-MATIC RESULTS” window. Individual values for the chunks are in the “Results” window. The “Vessel distribution” graph is a cumulative histogram of the vascular diameter in relation to length.


<b>HOW TO USE THE CHUNK ANALYZER</b>

<b>Introduction:</b> This part of the analysis uses the VasOMatic_Chunk_Analyzer.R script, which runs on R 4.2.0. Chunk data should be stored in a .csv file with headers organized as : Subject, Group, Replicate, ROI, Diameter, Length, Coverage, Signal. The 4 first are for identification purposes, fill with ones if unused. The 4 last are the direct output from VasOMatic. The script requires the following R packages: rlang, vctrs, data.table, ggplot2, forcats, svDialogs, abind and matrixStats.

* <b>Step 1</b>: Open the ChunkAnalyzer.R script in R (RGui or R Studio).

* <b>Step 2</b>: Run the whole script. Use Edit>Run All… in RGui, or Ctrl+Shift+Enter in R Studio.

<i>Note</i>: The script will ask you to download or update the required packages (rlang, vctrs, data.table, ggplot2, forcats, svDialogs, abind and matrixStats) if they are not installed. Select a CRAN mirror and proceed.

* <b>Step 3</b>: In the browser that opens, select the .csv file containing the chunks data to analyze. Data must be sorted as presented above.

* <b>Step 4</b>: Confirm adequate detection of the groups, subjects and ROIs.

* <b>Step 5</b>: Select action in the 'hub' window:
<b>New Dataset</b>: Returns to Step 3.
<b>Bin Dataset</b>: Groups the data in bins corresponding to vascular size for each subject on a given ROI, plots the results groupwise and export a .csv table ready for statistics.
<b>Scatterplot</b>: Creates density maps for a ROI showing the distribution of data for the different groups or of a selected subject, or plots all ROIs of a single group. Plots can be saved as png.
<b>Exit</b>: Ends the script.