//                 VASOMATIC                 //
// v 0.1 - 9 April 2024 - Dr Audrey Chagnot //
// Load the macro in FIJI/ImageJ using plugins>>macro>>install
// Open your image
// Press F1 to run

// Please note that VasOMatic is meant to run on
// large FOV (eg 300x300 um) of medium-thick (30-70um)
// microscopy slices with a large number of vessels.

macro "Vas-o-matic [f1]" {

// Recover Channel information
getDimensions(width, height, channels, slices, frames);
getPixelSize(unit, pixelWidth, pixelHeight);
area = width*height*pixelWidth*pixelHeight;
Projections=newArray("Average Intensity","Max Intensity","Min Intensity","Sum Slices","Standard Deviation","Median");
Channels=newArray("-None-");
for (i = 1; i < channels+1; i++) {num = toString(i,0); Channels = Array.concat(Channels, num); }

// Prompt dialogue for channel selection
Dialog.create("Channel allocation");
Dialog.addChoice("Channel for vessels:", Channels, "1");
Dialog.addChoice("Channel for paravascular:", Channels, "-None-");
//Dialog.addChoice("Channel for nuclei:", Channels, "-None-");
Dialog.addChoice("Projection type:", Projections, "Max Intensity");
Dialog.show();
VChannel=Dialog.getChoice();
PChannel=Dialog.getChoice();
//NChannel=Dialog.getChoice();
Proj = Dialog.getChoice();

// Split and rename
run("Duplicate...", "duplicate");
rename("TEMP");
run("Split Channels");
selectWindow("C"+VChannel+"-TEMP");
if (slices > 1) { run("Z Project...", "projection=["+Proj+"]"); }
rename("Vessels");
if (PChannel != "-None-") {
selectWindow("C"+PChannel+"-TEMP");
if (slices > 1) { run("Z Project...", "projection=["+Proj+"]"); }
rename("Paravascular");
}

// Close other windows
if (isOpen("C1-TEMP")){ selectWindow("C1-TEMP"); run("Close"); }
if (isOpen("C2-TEMP")){ selectWindow("C2-TEMP"); run("Close"); }
if (isOpen("C3-TEMP")){ selectWindow("C3-TEMP"); run("Close"); }
if (isOpen("C4-TEMP")){ selectWindow("C4-TEMP"); run("Close"); }

// MASK for vessel

Satisfied=0;
j=0;
while (Satisfied == 0){
	// Cleanout of images from previous runs. Rinse and repeat.
	if (isOpen("Vessels mask")){ selectWindow("Vessels mask"); run("Close"); }
	if (isOpen("Vessels maskneg")){ selectWindow("Vessels maskneg"); run("Close"); }
	if (isOpen("Vessels maskedneg")){ selectWindow("Vessels maskedneg"); run("Close"); }
	if (isOpen("Vessels masked")){ selectWindow("Vessels masked"); run("Close"); }
	if (isOpen("Vessels mask overlap")){ selectWindow("Vessels mask overlap"); run("Close"); }
	
	Dialog.create("Mask adjustments");
	Dialog.addNumber("Largest particle", 500);
	Dialog.addNumber("Largest hole", 500);
	Dialog.addNumber("Max gap closure range", 3);
	Dialog.addNumber("Smoothing", 3);
	Dialog.show();
	LargestParticle = Dialog.getNumber();
	LargestHole = Dialog.getNumber();
	DECycles = Dialog.getNumber();
	MedianFilter = Dialog.getNumber();
	
	selectWindow("Vessels");
	run("Duplicate...", "title=[Vessels premask]");
	run("Median...", "radius="+MedianFilter);
	setAutoThreshold("Huang dark");
	run("Threshold...");
	waitForUser("Select vessels threshold (suggested: Huang). Press OK when done");
	setOption("BlackBackground", false);
	run("Convert to Mask");
	beep();


	// Removing particles
	run("Analyze Particles...", "size="+LargestParticle+"-Infinity pixel show=Masks clear");

	// Closing gaps
	for(i = 0; i < DECycles; i++) {
		run("Dilate");
	}

	// Closing holes
	run("Invert");
	rename("Vessels premask2");
	if (isOpen("Vessels premask")){ selectWindow("Vessels premask"); run("Close"); }
	run("Analyze Particles...", "size="+LargestHole+"-Infinity pixel show=Masks clear");
	run("Invert");
	
	// Readjusting and smoothing
	for(i = 0; i < DECycles; i++) {
		run("Erode");
	}
	run("Median...", "radius="+MedianFilter+"");
	rename("Vessels mask");
	if (isOpen("Vessels premask2")){ selectWindow("Vessels premask2"); run("Close"); }
	
	// Display for user
	run("Macro...", "code=[if(v==255)v=1;]");
	run("Duplicate...", "title=[Vessels maskneg]");
	run("Macro...", "code=[v=-v+1;]");
	imageCalculator("Multiply create 32-bit", "Vessels maskneg","Vessels");
	rename("Vessels maskedneg");
	imageCalculator("Multiply create 32-bit", "Vessels mask","Vessels");
	rename("Vessels masked");
	run("Merge Channels...", "c1=[Vessels masked] c2=[Vessels maskedneg] create keep");
	rename("Vessels mask overlap");
	run("Macro...", "code=[if(v<1)v=NaN;]");
	Stack.setChannel(1);
	run("Enhance Contrast", "saturated=0.35");
	Stack.setChannel(2);
	run("Grays");
	run("Enhance Contrast", "saturated=0.35");
	beep();
	
	if (j==0){
	Satisfied = getBoolean("Are you happy with this mask? (Vessels=Red | Background=Gray)?");
	} else {
	Satisfied = getBoolean("Hope this one is better? (Vessels=Red | Background=Gray)?");
	}
	j=1;
}

// Close other windows
if (isOpen("Vessels maskneg")){ selectWindow("Vessels maskneg"); run("Close"); }
if (isOpen("Vessels maskedneg")){ selectWindow("Vessels maskedneg"); run("Close"); }
if (isOpen("Vessels masked")){ selectWindow("Vessels masked"); run("Close"); }

selectWindow("Vessels mask");
run("Macro...", "code=[if(v==1)v=255;]"); // Sadly binaries are 0 - 255
run("Duplicate...", "title=[Skeleton]");
run("Skeletonize");

// PERICYTE MASK (or whatever on the vessels, e.g. AAVs)

Satisfied=0;
j=0;
while (Satisfied == 0){
	// Cleanout of images from previous runs. Rinse and repeat.
	if (isOpen("Paravascular mask")){ selectWindow("Paravascular mask"); run("Close"); }
	if (isOpen("Paravascular maskneg")){ selectWindow("Paravascular maskneg"); run("Close"); }
	if (isOpen("Paravascular maskedneg")){ selectWindow("Paravascular maskedneg"); run("Close"); }
	if (isOpen("Paravascular masked")){ selectWindow("Paravascular masked"); run("Close"); }
	if (isOpen("Paravascular mask overlap")){ selectWindow("Paravascular mask overlap"); run("Close"); }
	
	selectWindow("Paravascular");
	run("Duplicate...", "title=[Paravascular premask]");
	setAutoThreshold("Percentile dark");
	run("Threshold...");
	waitForUser("Select paravascular threshold (suggested: Percentile). Press OK when done");
	setOption("BlackBackground", false);
	run("Convert to Mask");
	beep();
	rename("Paravascular mask");

	// Display for user
	run("Macro...", "code=[if(v==255)v=1;]");
	run("Duplicate...", "title=[Paravascular maskneg]");
	run("Macro...", "code=[v=-v+1;]");
	imageCalculator("Multiply create 32-bit", "Paravascular maskneg","Paravascular");
	rename("Paravascular maskedneg");
	imageCalculator("Multiply create 32-bit", "Paravascular mask","Paravascular");
	rename("Paravascular masked");
	run("Merge Channels...", "c1=[Paravascular masked] c2=[Paravascular maskedneg] create keep");
	rename("Paravascular mask overlap");
	run("Macro...", "code=[if(v<1)v=NaN;]");
	Stack.setChannel(1);
	run("Enhance Contrast", "saturated=0.35");
	Stack.setChannel(2);
	run("Grays");
	run("Enhance Contrast", "saturated=0.35");
	beep();
	
	if (j==0){
	Satisfied = getBoolean("Are you happy with this mask? (Paravascular=Red | Background=Gray)?");
	} else {
	Satisfied = getBoolean("Hope this one is better? (Paravascular=Red | Background=Gray)?");
	}
	j=1;
}

// Close other windows
if (isOpen("Paravascular maskneg")){ selectWindow("Paravascular maskneg"); run("Close"); }
if (isOpen("Paravascular maskedneg")){ selectWindow("Paravascular maskedneg"); run("Close"); }
if (isOpen("Paravascular masked")){ selectWindow("Paravascular masked"); run("Close"); }
selectWindow("Paravascular mask");
run("Macro...", "code=[if(v==1)v=255;]"); // Sadly binaries are 0 - 255

// SKELETON 

selectWindow("Skeleton");
// Compute neighbors number, pixel tortuosity and true length
run("Macro...", "code=[if(v==255)v=1;]");
run("32-bit"); // We need 32-bit for the convolutions, though I don't understand why.
run("Duplicate...", "title=[Convolved]");
run("Convolve...", "text1=[1 1 1\n1 0 1\n1 1 1\n]"); // Neighbour kernel, encode the number of neighbors
imageCalculator("Multiply create 32-bit", "Skeleton","Convolved");
rename("Neighbor number");
if (isOpen("Convolved")){ selectWindow("Convolved"); run("Close"); }
selectWindow("Skeleton");
run("Duplicate...", "title=[Convolved]");
run("Convolve...", "text1=[128 64 32\n16 0 8\n4 2 1\n]");
imageCalculator("Multiply create 32-bit", "Skeleton","Convolved");
rename("Neighbor profile");
if (isOpen("Convolved")){ selectWindow("Convolved"); run("Close"); }
selectWindow("Neighbor profile");
run("Duplicate...", "title=[Tortuosity]");
run("Macro...","code=[if(v==129 || v==66 || v==36 || v==24) v=1; "+
"else if(v==136 || v==34 || v==17 || v==68 || v==12 || v==130 || v==48 || v==65) v=1.08; "+
"else if(v==160 || v==33 || v==5 || v==132) v=1.41; "+
"else v=NaN]");
selectWindow("Neighbor profile");
run("Duplicate...", "title=[True Length]");
run("Macro...","code=[if(v==66 || v==24) v=1; "+
"else if(v==136 || v==34 || v==17 || v==68 || v==12 || v==130 || v==48 || v==65) v=1.205; "+
"else if(v==160 || v==33 || v==5 || v==132 || v==129 || v==36) v=1.41; "+
"else v=NaN]");

// For calculating diameter
selectWindow("Vessels mask");
run("Duplicate...", "title=[Distance in Vessel]");
run("Distance Map");
imageCalculator("Multiply create 32-bit", "Skeleton","Distance in Vessel");
run("Macro...", "code=[if(v==0) v=NaN]");
rename("Diameter Map");

// MINIFACS

// Vessel fragmentation, add to manager
selectWindow("Vessels mask");
run("Duplicate...", "title=[ROIs]");
run("Watershed");
roiManager("reset");
run("Analyze Particles...", "size=0-Infinity pixel show=Nothing add");
NPlots = roiManager("count");

// Get vessel geometries
roiManager("Deselect");
run("Set Measurements...", "area mean redirect=None decimal=3");
selectWindow("Diameter Map");
roiManager("multi-measure append");
Diameter = Table.getColumn("Mean");
for (i=0; i < NPlots ; i++) {Diameter[i] = 2*Diameter[i]*pixelWidth;}
roiManager("Deselect");
if (isOpen("Results")){ selectWindow("Results"); run("Close"); }
selectWindow("True Length");
run("Set Scale...", "distance=0 known=0 unit=pixel");
roiManager("multi-measure append");
LengthM = Table.getColumn("Mean");
LengthA = Table.getColumn("Area");
Length=newArray();
for (i=0; i < NPlots ; i++) {Length[i] = LengthM[i]*LengthA[i]*pixelWidth;}

// Get second channel signal
roiManager("Deselect");
if (isOpen("Results")){ selectWindow("Results"); run("Close"); }
selectWindow("Paravascular");
roiManager("multi-measure append");
PVasc = Table.getColumn("Mean");

// Do blob-wise coverage analysis
roiManager("Deselect");
if (isOpen("Results")){ selectWindow("Results"); run("Close"); }
selectWindow("Paravascular mask");
roiManager("multi-measure append");
Cov = Table.getColumn("Mean");
for (i=0; i < NPlots ; i++) {Cov[i] = 100*Cov[i]/255;} // Convert to % of coverage

// Remove NaNs
for (i=NPlots-1; i > 0 ; i--) {
	if (Diameter[i]*0 != 0 || Length[i]*0 != 0) {
	Diameter = Array.deleteIndex(Diameter, i);
	Length = Array.deleteIndex(Length, i);
	PVasc = Array.deleteIndex(PVasc, i);
	Cov = Array.deleteIndex(Cov, i);
	}
}

// Sort arrays by diameter and do basic statistics
OrdDiameter = Diameter; OrdLength = Length; OrdPVasc = PVasc; OrdCov = Cov;
Array.sort(OrdDiameter,OrdLength, OrdPVasc, OrdCov);
Array.getStatistics(OrdDiameter, min, max, mean, stdDev);
meanDiam = mean;
minDiam = min;
maxDiam = max;
stdDiam = stdDev;
Array.getStatistics(OrdLength, min, max, mean, stdDev);
totLength = mean*Length.length;
Array.getStatistics(OrdCov, min, max, mean, stdDev);
coverage = mean;

// Close windows
if (isOpen("Paravascular")){ selectWindow("Paravascular"); run("Close"); }
if (isOpen("Paravascular mask")){ selectWindow("Paravascular mask"); run("Close"); }
if (isOpen("Diameter Map")){ selectWindow("Diameter Map"); run("Close"); }
if (isOpen("True Length")){ selectWindow("True Length"); run("Close"); }
if (isOpen("ROIs")){ selectWindow("ROIs"); run("Close"); }
if (isOpen("Vessel mask")){ selectWindow("Vessel mask"); run("Close"); }
if (isOpen("Distance in Vessel")){ selectWindow("Distance in Vessel"); run("Close"); }
if (isOpen("Neighbor profile")){ selectWindow("Neighbor profile"); run("Close"); }
if (isOpen("Tortuosity")){ selectWindow("Tortuosity"); run("Close"); }
if (isOpen("Neighbor number")){ selectWindow("Neighbor number"); run("Close"); }
if (isOpen("Skeleton")){ selectWindow("Skeleton"); run("Close"); }
if (isOpen("Paravascular mask overlap")){ selectWindow("Paravascular mask overlap"); run("Close"); }
if (isOpen("Vessels mask overlap")){ selectWindow("Vessels mask overlap"); run("Close"); }
if (isOpen("Vessels")){ selectWindow("Vessels"); run("Close"); }

// DISPLAY RESULTS

// Display summary
print("=====================   VAS-O-MATIC RESULTS   =====================\n"+
"FOV: "+(width*pixelWidth)+" on "+(height*pixelHeight)+" "+unit+" ("+width+" x "+height+" pixels)\n"+
"\n"+
"On "+NPlots+" vessel chunks,\n"+
"Total Length: "+totLength+" "+unit+"\n"+
"Diameter: "+meanDiam+" +/- "+stdDiam+" "+unit+" (min: "+minDiam+", max: "+maxDiam+" "+unit+")\n"+
"Coverage: "+coverage+" %\n\n"+
NPlots+"\n"+
totLength+"\n"+
meanDiam+"\n"+
stdDiam+"\n"+
coverage+"\n"+
"");

// Show structure
// cumsum
slidingsum=0;
cumlength=newArray(); // Will be in mm for ease of representation
for (x = 0; x < Length.length; x++) {slidingsum += Length[x]/1000;	cumlength = Array.concat(cumlength,slidingsum);}
Plot.create("Vessels distribution", "Length (mm)", "Diameter (um)", cumlength, Diameter);
Plot.show();

// Display results
run("Clear Results");
row = 0;
for (i=0; i < Diameter.length; i++) {
    setResult("Diameter", row, OrdDiameter[i]);
    setResult("Length", row, OrdLength[i]);
    setResult("Coverage", row, OrdCov[i]); 
    setResult("Paravascular signal", row, OrdPVasc[i]);
    row++;
   }
updateResults();

}